;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (Linux)
;--------------------------------------------------------
; PIC port for the 14-bit core
;--------------------------------------------------------
;	.file	"codigo.c"
	list	p=16f84
	radix dec
	include "p16f84.inc"
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	extern	_TRISB
	extern	_TRISA
	extern	_PORTB
	extern	_PORTA
	extern	__sdcc_gsinit_startup
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	global	_main
	global	_i

	global PSAVE
	global SSAVE
	global WSAVE
	global STK12
	global STK11
	global STK10
	global STK09
	global STK08
	global STK07
	global STK06
	global STK05
	global STK04
	global STK03
	global STK02
	global STK01
	global STK00

sharebank udata_ovr 0x000C
PSAVE	res 1
SSAVE	res 1
WSAVE	res 1
STK12	res 1
STK11	res 1
STK10	res 1
STK09	res 1
STK08	res 1
STK07	res 1
STK06	res 1
STK05	res 1
STK04	res 1
STK03	res 1
STK02	res 1
STK01	res 1
STK00	res 1

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
UD_codigo_0	udata
_i	res	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized absolute data
;--------------------------------------------------------
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
STARTUP	code 0x0000
	nop
	pagesel __sdcc_gsinit_startup
	goto	__sdcc_gsinit_startup
;--------------------------------------------------------
; code
;--------------------------------------------------------
code_codigo	code
;***
;  pBlock Stats: dbName = M
;***
;has an exit
;; Starting pCode block
S_codigo__main	code
_main:
; 2 exit points
;	.line	15; "codigo.c"	TRISA = TRISB = 0;
	BANKSEL	_TRISB
	CLRF	_TRISB
	CLRF	_TRISA
;	.line	18; "codigo.c"	PORTA = 0;
	BANKSEL	_PORTA
	CLRF	_PORTA
;	.line	19; "codigo.c"	PORTB = 0;
	CLRF	_PORTB
_00108_DS_:
;	.line	23; "codigo.c"	PORTB = 0b00010000;  /* put 5V on pin 9 */
	MOVLW	0x10
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	27; "codigo.c"	for(i=0; i<40000; i++);
	MOVLW	0x40
	MOVWF	_i
	MOVLW	0x9c
	MOVWF	(_i + 1)
_00112_DS_:
	MOVLW	0xff
	ADDWF	_i,F
	BTFSS	STATUS,0
	DECF	(_i + 1),F
	MOVF	(_i + 1),W
	IORWF	_i,W
	BTFSS	STATUS,2
	GOTO	_00112_DS_
;	.line	29; "codigo.c"	PORTB = 0b00001000;  /* put 5V on pin 10 */
	MOVLW	0x08
	BANKSEL	_PORTB
	MOVWF	_PORTB
;	.line	31; "codigo.c"	for(i=0; i<40000; i++);  /* pause again */
	MOVLW	0x40
	MOVWF	_i
	MOVLW	0x9c
	MOVWF	(_i + 1)
_00115_DS_:
	MOVLW	0xff
	ADDWF	_i,F
	BTFSS	STATUS,0
	DECF	(_i + 1),F
	MOVF	(_i + 1),W
	IORWF	_i,W
	BTFSS	STATUS,2
	GOTO	_00115_DS_
	MOVLW	0x40
	MOVWF	_i
	MOVLW	0x9c
	MOVWF	(_i + 1)
	GOTO	_00108_DS_
;	.line	33; "codigo.c"	}
	RETURN	
; exit point of _main


;	code size estimation:
;	   38+    4 =    42 instructions (   92 byte)

	end
